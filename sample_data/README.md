# Weather to Test - Sample Data

## Data description

The `temp_adjustment.csv` file has, for the tests in our sample, the following data fields:
1. `test_name`,	`test_loinc` - the name and LOINC code of the test
2. `test_medium`,	`test_units` - the medium (e.g., blood, urine) and units of the sample
3. `temp_effect_c`	, `temp_effect_se` - the adjustment, and the standard error (both from the linear version of the adjustment procedure, which we view as more stable than the temperature bins)	 
4. `frac_upper_limit_cv` - the degree of imprecision induced by temperature, as a fraction of maximum allowable imprecision tolerated by laboratory performance guidelines (from: Ricós C, Alvarez V, Cava F, et al. Current databases on biological variation: pros, cons and progress. Scand J Clin Lab Invest 1999;59(7):491–500).


## Procedure description

We provide a sample file, `sample_data.csv`, with two hypothetical LDL measurements taken at different temperatures. The procedure, described in the notebook `demo.ipynb`, performs the following steps:

1. Load in both files, and merge in the adjustment data fields to the sample data using the LOINC code.
2. Perform checks that the medium and units match between the sample and adjustment data.
3. Optionally, check that the adjustment is significant (i.e., abs(`temp_effect_c` / `temp_effect_se`) > 2) and that it accounts for a sufficient degree of imprecision (`frac_upper_limit_cv` > 0.05) to make adjustment worthwhile.
3. Define a standard temperature (we choose 20°C).
4. Perform the adjustment. Concretely, take the difference between the recorded temperature of the sample and the standard temperature, multiply by the linear adjustment factor, and subtract this from the test result. The result is the adjusted result, at the standard temperature.
