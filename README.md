# Weather to Test
Created by Devin Pope - August 7, 2021

There are 5 `.do` files that are used to create the results:

- `0-Master-Weather-General.do`: Sets globals
- `1-Lab-Weather-Merge.do`: Cleans weather data and merges with lab test data
- `2-Replicate-Devin.do`: For each test, produces temperature X test-result interactions graphically and using regression
- `3-Put-PDF.do`: Places figures into pdf for paper and appendix
- `4-Reg-Table.do`: Creates regression table

The lab data for this project is proprietary and confidential and thus cannot be posted.
Contact author Devin Pope (devin.pope@chicagobooth.edu) for more information about data access.

However, we provide a sample dataset in `sample_data` as a more concrete example.
