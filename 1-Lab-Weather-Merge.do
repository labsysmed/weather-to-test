* ------------------------------------------------------------ *
* 1-Lab-Weather-Merge.do
* Build merged test and temperature data for top 100 tests
* ------------------------------------------------------------ *

version 14
clear all
set more off
capture log close

* ------------------------------------------------------------ *
* Clean the raw weather data
* The weather data may have repeated observations for some MSA-Date
* We rid of duplicates by taking simple average
* ------------------------------------------------------------ *

capture mkdir "$data_extract_dir/Cleaned-General"

capture confirm file "$data_extract_dir/Cleaned-General/Weather-Long.dta"
if _rc != 0 {
	use "$weather_dir/msa_weather_data.dta", clear
	rename cbsa_code msa
	keep msa tmax date
	collapse (mean) tmax, by(msa date) fast
	sort msa date, stable
	save "$data_extract_dir/Cleaned-General/Weather-Long.dta", replace
}

capture confirm file "$data_extract_dir/Cleaned-General/Weather-Wide.dta"
if _rc != 0 {
	use "$weather_dir/msa_weather_data_wide.dta", clear
	rename cbsa_code msa
	keep msa date lg* ld*
	collapse (mean) lg* ld*, by(msa date) fast
	sort msa date, stable
	save "$data_extract_dir/Cleaned-General/Weather-Wide.dta", replace
}

* ------------------------------------------------------------ *
* For each of the top 100 tests, stack lab obs from all years 
* Then merge with weather data
* CAUTION: This part of the code takes 3-4 days to run on the full data
* ------------------------------------------------------------ *

capture mkdir "$data_extract_dir/Weather-Project"
capture mkdir "$data_extract_dir/Weather-Project/General"
capture mkdir "$data_extract_dir/Weather-Project/Temporary" 

local extract_output = "$data_extract_dir/Weather-Project/General"
local temp_output = "$data_extract_dir/Weather-Project/Temporary" 
local wide_weather_path = "$data_extract_dir/Cleaned-General/Weather-Wide.dta"

* Loop through 100 tests; for each test, loop through 7 years

local counter 1
local totalcount = 7*$TestCount
foreach test in $TopTests {
	* If a test has already been extracted, skip
	capture confirm file "`extract_output'/`test'.dta"
	if _rc != 0 {
		di "(`counter'/`totalcount') Aggregated data for test: `test' does not exist. Processing..."
		* Stack data for that test over years
		* The code here can be improved for speed
		foreach yr in 09 10 11 12 13 14 15 {
			di "(`counter'/`totalcount') Loading 20`yr' data for test: `test'"
			use "$medical_dir/l/l`yr'.dta" if loinccd == "`test'", clear
			local counter = `counter'+1
			save "`temp_output'/Y`yr'_`test'.dta", replace
		}
		foreach yr in 09 10 11 12 13 14 {
			append using "`temp_output'/Y`yr'_`test'.dta"
		}
		* Merge weather to the stacked data and save
		keep loinccd enrolid abnormal result msa svcdate resunit
		rename svcdate date
		merge m:1 msa date using "`wide_weather_path'", keep(match master) nogen
		save "`extract_output'/`test'.dta", replace
		* Housekeeping: remove temporary files
		foreach yr in 09 10 11 12 13 14 15 {
			capture rm "`temp_output'/Y`yr'_`test'.dta"
		}
	}
	else {
		di "(`counter'/`totalcount') Aggregated data for test: `test' exists. Skipping..."
		local counter = `counter'+7
	}
}
* Remove the temporary output directory
capture rmdir "`temp_output'"


