* ------------------------------------------------------------ *
* Master-Weather-General.do
* Master script for building test-level data and running analysis
* ------------------------------------------------------------ *

* ------------------------------------------------------------ *
* 1. Build merged test and temperature data
* 2. Replicate Devin's analysis on the temperature-result relation
* 3. Extend Devin's analysis using abnormal flags
* ------------------------------------------------------------ *

version 14
clear all
set more off
capture log close

* ------------------------------------------------------------ *
* User and Directory Settings
* ------------------------------------------------------------ *

global user_of_this_code KK

if "$user_of_this_code" == "KK" {
	global master_dir D:/Analysis
	global data_dir D:/Raw Data
	global TOL_HDFE 1e-4
} 
if "$user_of_this_code" == "DP" {
	global master_dir I:/Analysis
	global data_dir I:/Raw Data
	global TOL_HDFE 1e-6
}

cd "$master_dir"

global script_dir  		Scripts/General
global program_dir 		Scripts/Custom-Programs
global crosswalk_dir    Scripts/Crosswalk
global data_extract_dir Data-Extract
global result_dir 		Results
global log_dir 			Logs
global medical_dir 		$data_dir/Medical Data
global weather_dir 		$data_dir/Weather Data

capture mkdir "$result_dir"
capture mkdir "$log_dir"

discard
adopath + $program_dir

* ------------------------------------------------------------ *
* Read in LOINC codes for interesting tests
* ------------------------------------------------------------ *

* List of lab tests to use
global bad_tests 9318-7 1759-0 706-2 11054-4 713-8 785-6 27353-2 48159-8 736-9 5905-5 770-8 5334-8 32215-6 9830-1 3097-3 20405-7 5767-9 630-4 5769-5 5770-3 33255-1 5778-6 19767-3 47527-7 19774-9 3145-0 3050-2 11277-1 13945-1 33914-3 48642-3 48643-1 25428-4 5794-3 30167-1 5796-8 38518-7 2514-8 5799-2 5821-4 10524-7 5802-4 22637-3 20454-5 8251-1 19763-2 31208-2 19764-0 2991-8 11579-0 883-9 3298-7 3321-7 43409-2 6463-4 28037-0 27391-2 21613-5 43304-5 3474-4 3208-6 6077-2 5146-6 30428-7 5808-1 32776-7 6121-8 19762-4 2349-9 17865-7 5196-1 13955-0 7420-3 3683-0 51584-1 6166-3 32093-7 12235-8 43305-2 3862-0 42186-7 19139-5 19769-9 22634-0 2748-2 6927-8 6853-6 20507-0 16017-6 2965-2 40967-2 40968-0 18500-9 3051-0 21582-2
global crosswalk_path $crosswalk_dir/Loinc/Crosswalk-loinc-top170.dta
use $crosswalk_path, clear

* We take out some tests that are not amenable to our analysis (no data, different units, etc.)
foreach test in $bad_tests {
	drop if loinccd=="`test'"
}

* Put all test codes into a global variable
global TopTests ""
qui count
global TestCount = `r(N)'
forvalues i = 1/$TestCount {
	local code = loinccd[`i']
	global TopTests $TopTests `code'
}
di "$TopTests" // Shows what this variable looks like
di "$TestCount"

clear

* ------------------------------------------------------------ *
* Set Global Parameters
* ------------------------------------------------------------ *

* Set global parameters for analysis
global TRIM_PERC_LOW 	2.5     // Lower trimming percentile
global TRIM_PERC_HIGH 	97.5    // Higher trimming percentile
global TEMPERATURE_BIN 	5		// Temperture bin width
global TOP_MSA 			0 		// Top MSA to include (set to 0 for all MSAs)
global white			plotregion(fcolor(white) lcolor(white) m(small)) graphregion(fcolor(white) lcolor(white))

/* ------------------------------------------------------------ *
* Source data build and analysis scripts in order
* ------------------------------------------------------------ *

* 1. Build merged test and temperature data
do "$script_dir/1-Lab-Weather-Merge.do"

* 2. Replicate Devin's analysis on the temperature-result relation
do "$script_dir/2-Replicate-Devin.do"

