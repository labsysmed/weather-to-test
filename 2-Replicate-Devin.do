/*______________________________________________________________________________

	2-Replicate-Devin.do
	Replicate Devin's analysis on the temperature-result relation
		For each test, make the following plots:
		1. Count by month
		2. Result by month, using patient FE
		3. Count by temperature bucket
		4. Regress result on temperature bins, using patient + msa*week FE
		5. Regress result on lead/lag temperatures, using patient + msa*week FE
______________________________________________________________________________*/
global TopTests	1989-3

version 14
clear all
set more off
capture log close

* ------------------------------------------------------------ *
* Loop through tests and do analysis
* ------------------------------------------------------------ *
* Initialize a counter to keep track of test iterations
local m = 1

foreach test in $TopTests {
	
	* Logging
	capture log close
	log using "$log_dir/Replicate-Devin-`test'.smcl", replace

	* Find test name
	use "$crosswalk_path" if loinccd == "`test'", clear
	local testname = test[1]
	di "Processing test `test': `testname'"
	
	* Create Test-Specific Output Directory
	local output_dir "$result_dir/`test'_`testname'"
	capture mkdir "`output_dir'"
	
	*capture confirm file "`output_dir'/Result-Distribution.png"
	*if _rc!=0 {
	
	* ------------------------------------------------------------ *
	* More Data Cleaning: Generate Data Trimming Flags
	* (1) Trim outliers by percentile
	* (2) Drop missing values
	* ------------------------------------------------------------ *
	capture noisily {
		* Load Merged Weather-Test Data
		use "$data_extract_dir/Weather-Project/General/`test'.dta", clear
		drop if lg_0==. | result==. | result==0
		duplicates drop 
		
		* At this point, fix any weird result distribution due to unit changes
		if "`test'"=="751-8" { // Neutrophils: average ranges around 3000
			replace result=result*10000000 if inrange(result, 0.0001, 0.001)
			replace result=result*100000 if inrange(result, 0.01, 0.1)
			replace result=result*1000 if inrange(result, 1, 10)
			replace result=result*100 if inrange(result, 10, 100)
		}
		if "`test'"=="742-7" { // Monocytes: average ranges around 200-600
			replace result=result*10000000 if inrange(result, 0.0001, 0.001)
			replace result=result*100000 if inrange(result, 0.001, 0.01)
			replace result=result*1000 if inrange(result, 0.1, 1)
			replace result=result*100 if inrange(result, 1, 10)
		}
		if "`test'"=="731-0" { // Lymphocytes: average ranges around 1500
			replace result=result*100000 if result<.09
			replace result=result*1000 if inrange(result, .09, 10)
			replace result=result*100 if inrange(result, 10, 100)
		}
		if "`test'"=="704-7" { // Basophils: average ranges around 30
			drop if result<.1 // a lot of ~0.1 results, drop
			replace result=result*100 if inrange(result, .1, 1)
		}
		if "`test'"=="711-2" { // Eosinophils: average ranges around 100 (normal is less than 500)
			replace result=result*100000 if inrange(result, 0, .01)
			replace result=result*1000 if inrange(result, 0.09, 1)
		}
		if "`test'"=="18262-6" { // LDL by Direct Assay: average ranges around 100
			drop if result<1 // a lot of low results, drop
		}
		if "`test'"=="43396-1" { // Cholesterol NON-HDL: average ranges around 130
			drop if result<10 // a lot of low results, drop
		}
		if "`test'"=="2500-7" { // Iron binding capacity: average ranges around 300
			drop if result<50 // a lot of low results, drop
		}
		if "`test'"=="2161-8" { // Creatinine in Urine: average ranges around 100
			replace result=result*100 if result<5
		}
		if "`test'"=="5334-8" { // Rubella virus: average ranges around 20; CHECK WITH DEVIN
			replace result=result*10 if result<5
		}
		if "`test'"=="3209-4" { // Coagulation factor VIII
			replace result=result*10 if result<10
		}
		
		* (1) Trim outliers - Limit by percentile
		_pctile result, percentiles($TRIM_PERC_LOW $TRIM_PERC_HIGH)
		gen to_keep = result >= `r(r1)' & result <= `r(r2)'
		* At this moment, drop bad observations
		capture noisily drop if to_keep == 0  
		
		* (2) Trim to MSAs \\ $TOP_MSA = 0 indicates no MSA trimming
		drop if msa==. | msa==0
		gen top_msa_flag = 1
		
		* Get month and week of year
		gen month = month(date)
		gen year = year(date)
		replace month = month + (year-2009)*12 - 1
		gen week = week(date)
		
		* Merge with enrollment data to get date of birth
		merge m:1 enrolid using "$medical_dir/a/dobyr_all_enrollment.dta", keepusing(dobyr) keep(matched master) nogen
			
		* Make an age variable
		gen age = year - dobyr
	}
	
	* ------------------------------------------------------------ *
	* Analysis 1: Result by week, Patient FE
	* ------------------------------------------------------------ *
	* Regress result on week dummy
	capture confirm file "`output_dir'/Result-By-Week-PatientYear-Cluster.pdf"
	if _rc != 0 { 
		preserve
		capture noisily {
			* Regress result on month dummies, absorbing patient FE
			reghdfe result i.week, absorb(enrolid year) vce(cluster msa#date) poolsize(20) timeit
			* Collect the regression coefficients into the data table
			parmest, fast
			* Process the coefficients and CI, make plot
			gen week = regexs(0) if regexm(parm, "^[0-9]+")
			destring week, force replace
			drop if stderr == 0 | week == .
			twoway (rcap min95 max95 week, msize(tiny)) ///
			(scatter estimate week, msize(small)), legend(off) $white ///
			title("Average Test Result By Week-of-Year", size(medsmall)) ///
			subtitle("Patient and Year Fixed Effect; MSA*Date Clustering", size(small)) ///
			xlabel(0(4)52) xscale(range(1 53)) ///
			xtitle("", size(small)) ylabel(, nogrid) ///
			ytitle("Result Relative to the First Week of the Year", size(small))
			graph export "`output_dir'/Result-By-Week-PatientYear-Cluster.pdf", replace
			graph export "`output_dir'/Result-By-Week-PatientYear-Cluster.png", replace
			* No title
			twoway (rcap min95 max95 week, msize(tiny)) ///
			(scatter estimate week, msize(small)), legend(off) $white ///
			title("",) ///
			xlabel(0(4)52) xscale(range(1 53)) ///
			xtitle("", size(small)) ylabel(, nogrid) ///
			ytitle("Result Relative to the First Week of the Year", size(small))
			graph export "`output_dir'/Result-By-Week-PatientYear-Cluster-NoTitle.png", replace

		}
		restore
	}
	
	* ------------------------------------------------------------ *
	* Temperature bins
	* ------------------------------------------------------------ *
	capture noisily {	
		* Convert temp into Celsius
		replace lg_0=(lg_0-32)*(5/9)
		foreach num of numlist 1/15 {
			replace lg_`num'=(lg_`num'-32)*(5/9)
			replace ld_`num'=(ld_`num'-32)*(5/9)
		}
		* Binning the temperature
		gen bin = $TEMPERATURE_BIN*floor(lg_0/$TEMPERATURE_BIN)
		replace bin = 0 if bin < 0
		replace bin = 35 if bin > 35
	}
	* ------------------------------------------------------------ *
	* Analysis 2a: Result on temperature bins, patient + msa*week FE
	* ------------------------------------------------------------ *
	capture confirm file "`output_dir'/Result-By-Bucket-MSAWeekPatientYear-Cluster.pdf"
	if _rc != 0 {
		preserve
		capture noisily {
			* Regress result on temperature bins, absorbing patient and msa*week-of-year FE
			label var result "`testname'"
			reghdfe result i.bin, absorb(msa#week enrolid year) vce(cluster msa#date) tol($TOL_HDFE) verbose(3) poolsize(20) timeit accel(sd)
			* For first iteration, create the output file. For subsequent iterations, append to it
			if(m==1) {
				outreg2 using "$result_dir/Tables/table-temperature-bucket-coefficients.xls", label sideway noaster replace
			} 
			else {
				outreg2 using "$result_dir/Tables/table-temperature-bucket-coefficients.xls", label sideway noaster append
			}
			
			local m = `m'+1
			
			* Collect coefficients and make plot
			parmest, fast
			gen bin = regexs(0) if regexm(parm, "^[0-9]+")
			destring bin, force replace
			drop if stderr == 0 | bin == .
			
			set obs `=_N+1'
			replace bin=0 if bin==.
			replace estimate=0 if estimate==.
			
			label define xaxis 0 "<5" 5 "5-10" 10 "10-15" 15 "15-20" 20 "20-25" 25 "25-30" 30 "30-35" 35 "35+"
			label values bin xaxis
			
			twoway (rcap min95 max95 bin, msize(tiny)) ///
			(scatter estimate bin, msize(small)), $white legend(off) ///
			title("Average Test Result By Temperature Bins", size(medsmall)) ///
			subtitle("Patient, MSA*Week-of-Year, and Year Fixed Effect; MSA*Date Clustering", size(small)) ///
			xlabel(0(5)35, valuelabel) xscale(r(-1 36)) ylabel(, nogrid) ///
			xtitle("5-Degree Temperature Bins (in Celsius)", size(small)) ///
			ytitle("Result Relative to <5 Degrees", size(small))
			graph export "`output_dir'/Result-By-Bucket-MSAWeekPatientYear-Cluster.pdf", replace
			graph export "`output_dir'/Result-By-Bucket-MSAWeekPatientYear-Cluster.png", replace
		
			twoway (rcap min95 max95 bin, msize(tiny)) ///
			(scatter estimate bin, msize(small)), $white legend(off) ///
			title("", size(medsmall)) ///
			xlabel(0(5)35, valuelabel) xscale(r(-1 36)) ylabel(, nogrid) ///
			xtitle("5-Degree Temperature Bins (in Celsius)", size(small)) ///
			ytitle("Result Relative to <5 Degrees", size(small))
			graph export "`output_dir'/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png", replace
		}
		restore
	}
	
	* ------------------------------------------------------------ *
	* Analysis 2b: Result on temperature bins, patient + msa*week FE, age
	* ------------------------------------------------------------ *
	capture confirm file "`output_dir'/Result-By-Bucket-MSAWeekPatientYear-Cluster-Age.pdf"
	if _rc != 0 {
		capture noisily {
			preserve
			* Regress result on temperature bins of just young patients
			reghdfe result i.bin if age<50, absorb(msa#week enrolid year) vce(cluster msa#date) tol($TOL_HDFE) verbose(3) poolsize(20) timeit accel(sd)
			* Collect coefficients and make plot
			parmest, fast
			gen bin = regexs(0) if regexm(parm, "^[0-9]+")
			destring bin, force replace
			drop if stderr == 0 | bin == .
			
			tempfile young
			save `young'
			restore 
			
			* Regress result on temperature bins of older patients
			preserve
			reghdfe result i.bin if age>=50, absorb(msa#week enrolid year) vce(cluster msa#date) tol($TOL_HDFE) verbose(3) poolsize(20) timeit accel(sd)
			* Collect coefficients and make plot
			parmest, fast
			gen bin = regexs(0) if regexm(parm, "^[0-9]+")
			destring bin, force replace
			drop if stderr == 0 | bin == .
			gen old=1
			
			set obs `=_N+1'
			replace bin=0 if bin==.
			replace estimate=0 if estimate==.
			replace old=2 if old==.
			
			append using `young'
			
			set obs `=_N+1'
			replace bin=0 if bin==.
			replace estimate=0 if estimate==.
			
			label define xaxis 0 "<5" 5 "5-10" 10 "10-15" 15 "15-20" 20 "20-25" 25 "25-30" 30 "30-35" 35 "35+"
			label values bin xaxis
			
			twoway (rcap min95 max95 bin if old==., msize(tiny)) ///
			(rcap min95 max95 bin if old==1, msize(tiny)) ///
			(scatter estimate bin if old==., msize(small)) ///
			(scatter estimate bin if old==1, msize(small)) ///
			(scatter estimate bin if old==2, msize(small) mcolor(black)), $white ///
			legend(order(3 "Age <50" 4 "Age 50-65")) ///
			title("Average Test Result By Temperature Bins", size(medsmall)) ///
			subtitle("Patient, MSA*Week-of-Year, and Year Fixed Effect; MSA*Date Clustering", size(small)) ///
			xlabel(0(5)35, valuelabel) xscale(r(-1 36)) ylabel(, nogrid) ///
			xtitle("5-Degree Temperature Bins (in Celsius)", size(small)) ///
			ytitle("Result Relative to <5 Degrees", size(small))
			graph export "`output_dir'/Result-By-Bucket-MSAWeekPatientYear-Cluster-Age.pdf", replace
			graph export "`output_dir'/Result-By-Bucket-MSAWeekPatientYear-Cluster-Age.png", replace
		
			twoway (rcap min95 max95 bin if old==., msize(tiny)) ///
			(rcap min95 max95 bin if old==1, msize(tiny)) ///
			(scatter estimate bin if old==., msize(small)) ///
			(scatter estimate bin if old==1, msize(small)) ///
			(scatter estimate bin if old==2, msize(small) mcolor(black)), $white ///
			legend(order(3 "Age <50" 4 "Age 50-65")) ///
			title("") ///
			xlabel(0(5)35, valuelabel) xscale(r(-1 36)) ylabel(, nogrid) ///
			xtitle("5-Degree Temperature Bins (in Celsius)", size(small)) ///
			ytitle("Result Relative to <5 Degrees", size(small))
			graph export "`output_dir'/Result-By-Bucket-MSAWeekPatientYear-Cluster-Age-NoTitle.png", replace
			
			restore
		}
	}
	
	* ------------------------------------------------------------ *
	* Analysis 3: Result on lead/lag temperature, patient + msa*week FE
	* ------------------------------------------------------------ *
	capture confirm file "`output_dir'/Result-LeadLag-MSAWeekPatientYear-Cluster.pdf"
	if _rc != 0 {
		preserve
		capture noisily {
			* Regress result on lead/lag temp, absorbing patient and msa*week-of-year FE
			reghdfe result lg_* ld_*, absorb(msa#week enrolid year) vce(cluster msa#date) tol($TOL_HDFE) verbose(3) poolsize(20) timeit accel(sd)
			* Collect coefficients and make plot
			parmest, fast
			gen time = regexs(0) if regexm(parm, "[0-9]+$")
			destring time, force replace
			replace time = -time if substr(parm, 1, 2) == "lg"
			
			twoway (rcap min95 max95 time, msize(tiny)) ///
			(scatter estimate time, msize(small)), legend(off) $white ///
			title("Coefficient On Lag/Lead Temperatures", size(medsmall)) ///
			subtitle("Patient, MSA*Week-of-Year, and Year Fixed Effect; MSA*Date Clustering", size(small)) ///
			xlabel(-15(1)15, labsize(small)) ylabel(, nogrid) ///
			xtitle("Days Relative to Test Date", size(small)) ///
			ytitle("Linear Regression Coefficient", size(small)) ///
			xline(0, lcolor(gs8)) yline(0, lcolor(gs8))
			graph export "`output_dir'/Result-LeadLag-MSAWeekPatientYear-Cluster.pdf", replace
			graph export "`output_dir'/Result-LeadLag-MSAWeekPatientYear-Cluster.png", replace
			
			twoway (rcap min95 max95 time, msize(tiny)) ///
			(scatter estimate time, msize(small)), legend(off) $white ///
			title("") ///
			xlabel(-15(1)15, labsize(small)) ylabel(, nogrid) ///
			xtitle("Days Relative to Test Date", size(small)) ///
			ytitle("Linear Regression Coefficient", size(small)) ///
			xline(0, lcolor(gs8)) yline(0, lcolor(gs8))
			graph export "`output_dir'/Result-LeadLag-MSAWeekPatientYear-Cluster-NoTitle.png", replace
		}
		restore
	}
	
	
	* ------------------------------------------------------------ *
	* Analysis 4: Result Distribution
	* ------------------------------------------------------------ *
	capture confirm file "`output_dir'/Result-Distribution.pdf"
	if _rc != 0 {
		* Some results are in a float variable. Round and change
		replace result = result*100
		replace result = round(result, 1)
		replace result = result/100
		
		* Save width for histogram as a local
		if "`test'"=="21198-7" {
			local width 5000
		}
		foreach width_group in 751-8 731-0 {
			if "`test'"=="`width_group'" {
				local width 100
			}
		}
		foreach width_group in 2132-9 9318-7 2243-4 {
			if "`test'"=="`width_group'" {
			local width 20
			}
		}
		foreach width_group in 742-7 9318-7 2276-4 2157-6 2986-8 {
			if "`test'"=="`width_group'" {
				local width 10
			}
		}
		foreach width_group in 777-3 711-2 2500-7 2571-8 2284-8 2161-8 11572-5 2498-4 5334-8 {
			if "`test'"=="`width_group'" {
				local width 5
			}
		}
		foreach width_group in 6768-2 2093-3 1754-1 1558-6 15067-2 2345-7 2324-2 2532-0 27353-2 13458-5 3209-4 {
			if "`test'"=="`width_group'" {
				local width 2 
			}
		}
		foreach width_group in 736-9 770-8 787-2 704-7 13457-7 2085-9 2951-2 18262-6 1988-5 1990-1 1989-3 1920-8 1742-6 2075-0 2028-9 43396-1 4537-7 3097-3 3094-0 5902-2 2502-3 2236-8 48159-8 14979-9 {
			if "`test'"=="`width_group'" {
				local width 1
			}
		}
		foreach width_group in 785-6 20570-8 4544-3 718-7 3026-2 {
			if "`test'"=="`width_group'" {
				local width 0.5
			}
		}
		foreach width_group in 1751-7 5905-5 2885-2 2823-3 17861-6 10834-0 19123-9 1975-2 706-2 1971-1 2777-1 776-5 786-4 713-8 1759-0 2857-1 3016-3 3024-7 4548-4 3084-1 11054-4 5803-2 17855-8 6690-2 6301-6 32215-6 788-0 9830-1 {
			if "`test'"=="`width_group'" {
				local width 0.1
			}
		}
		foreach width_group in 2160-0 789-8 {
			if "`test'"=="`width_group'" {
				local width 0.05
			}
		}
		foreach width_group in 1968-7 5811-5 {
			if "`test'"=="`width_group'" {
				local width 0.01
			}
		}
		
		* Make histogram
		tw hist result, $white percent xtitle("") title("Labs Result Distribution") ///
		subtitle("Width: `width'") width(`width') color(navy) fintensity(80) ///
		ylabel(, nogrid) discrete
		graph export "`output_dir'/Result-Distribution.pdf", replace
		//graph export "`output_dir'/Result-Distribution.png", replace
		
		tw hist result, $white percent xtitle("") title("") ///
		subtitle("Width: `width'") width(`width') color(navy) fintensity(80) ///
		ylabel(, nogrid) discrete
		graph export "`output_dir'/Result-Distribution-NoTitle.png", replace
	
		log close
	}
}
*}


/* Scraps
* Separate above graph by age groups <50 & 50+
	capture confirm file "`output_dir'/Result-By-Bucket-Age-MSAWeekPatientYear.pdf"
	if _rc != 0 {
		capture noisily {
			preserve
			* Regress result on temperature bins, absorbing patient and msa*week-of-year FE
			reghdfe result i.bin if age>=50, absorb(msa#week enrolid year) tol($TOL_HDFE) verbose(3) poolsize(20) timeit accel(sd)
			* Collect coefficients and make plot
			parmest, fast
			gen bin = regexs(0) if regexm(parm, "^[0-9]+")
			destring bin, force replace
			drop if stderr == 0 | bin == .
			gen young=0
			tempfile old
			save `old'
			restore
			
			preserve
			* Regress result on temperature bins, absorbing patient and msa*week-of-year FE
			reghdfe result i.bin if age<50, absorb(msa#week enrolid year) tol($TOL_HDFE) verbose(3) poolsize(20) timeit accel(sd)
			* Collect coefficients and make plot
			parmest, fast
			gen bin = regexs(0) if regexm(parm, "^[0-9]+")
			destring bin, force replace
			drop if stderr == 0 | bin == .
			gen young=1
			tempfile young
			save `young'
			restore
			
			preserve
			use `old', clear
			append using `young'
			
			label define xaxis 35 "35+"
			label values bin xaxis
			
			twoway (rcap min95 max95 bin if young==1, msize(tiny)) ///
			(rcap min95 max95 bin if young==0, msize(tiny)) ///
			(scatter estimate bin if young==1, msize(small)) ///
			(scatter estimate bin if young==0, msize(small)), ///
			$white  legend(order(3 "Ages <50" 4 "Ages 50+") nobox region(lstyle(none))) ///
			title("Average Test Result By Temperature Bins", size(medsmall)) ///
			subtitle("Patient and MSA*Week-of-Year Fixed Effect", size(small)) ///
			xlabel(5(5)35, valuelabel) xscale(r(4 36)) ylabel(, nogrid) ///
			xtitle("5-Degree Temperature Bins (in Celsius)", size(small)) ///
			ytitle("Result Relative to <5 Degrees", size(small))
			graph export "`output_dir'/Result-By-Bucket-Age-MSAWeekPatientYear.pdf", replace
			graph export "`output_dir'/Result-By-Bucket-Age-MSAWeekPatientYear.png", replace
			
			restore
		}
	}
	
	capture confirm file "`output_dir'/Age-By-Bucket-MSAWeekPatientYear.pdf"
	if _rc != 0 {
		preserve
		capture noisily {
			* Regress age on temperature bins, absorbing patient and msa*week-of-year FE
			reghdfe age i.bin, absorb(msa#week enrolid year) tol($TOL_HDFE) verbose(3) poolsize(20) timeit accel(sd)
			* Collect coefficients and make plot
			parmest, fast
			gen bin = regexs(0) if regexm(parm, "^[0-9]+")
			destring bin, force replace
			drop if stderr == 0 | bin == .
			
			label define xaxis 35 "35+"
			label values bin xaxis
			
			twoway (rcap min95 max95 bin, msize(tiny)) ///
			(scatter estimate bin, msize(small)), $white legend(off) ///
			title("Average Age By Temperature Bins", size(medsmall)) ///
			subtitle("Patient, MSA*Week-of-Year, and Year Fixed Effect", size(small)) ///
			xlabel(5(5)35, valuelabel) xscale(r(4 36)) ylabel(, nogrid) ///
			xtitle("5-Degree Temperature Bins (in Celsius)", size(small)) ///
			ytitle("Age Relative to <5 Degrees", size(small))
			graph export "`output_dir'/Age-By-Bucket-MSAWeekPatientYear.pdf", replace
			graph export "`output_dir'/Age-By-Bucket-MSAWeekPatientYear.png", replace

		}
		restore
	}





