/*______________________________________________________________________________

	3-Put-PDF.do
	Put the figures into one PDF (one page per test)

		* Author: Kristy Kim
		* Last Edited: 2018.10.30
______________________________________________________________________________*/

*--- CREATE MACROS FOR TEST ----*
* Get tests in alphabetical order
use $crosswalk_path, clear

* We take out some tests that are not amenable to our analysis (no data, different units, etc.)
foreach test in $bad_tests {
	drop if loinccd=="`test'"
}

* Put all test codes into a global variable
global TopTests ""
qui count
global TestCount = `r(N)'
replace test="PH of Urine" if test=="pH of Urine"
replace test="Specific Gravity of Urine" if test=="specific gravity of urine"
sort test
forvalues i = 1/$TestCount {
	local code = loinccd[`i']
	global TopTests $TopTests `code'
}
di "$TopTests" // Shows what this variable looks like
di "$TestCount"

clear

* Find test units for each test
capture confirm file "$data_extract_dir/Test Units/all_test_units.dta"
if _rc!=0 {
	foreach test in $TopTests {
		use "$data_extract_dir/Weather-Project/General/`test'.dta", clear
		keep loinccd resunit
		egen count = count(loinccd), by(resunit)
		gsort -count
		keep if _n==1
		save "$data_extract_dir/Test Units/`test'.dta", replace
	}
	clear
	foreach test in $TopTests {
		append using "$data_extract_dir/Test Units/`test'.dta"
	}
	save "$data_extract_dir/Test Units/all_test_units.dta", replace
}

local count=1

*_______________________________________________________________________________

*--- CREATE MAIN PDFs ---*	

* Warning: only compatible with Stata 15
foreach test in $TopTests {
	* Find test name
	use "$crosswalk_path" if loinccd == "`test'", clear
	local testname = test[1]
	di "Processing test `test': `testname'"
	
	* Create Test-Specific Output Directory
	local output_dir "$result_dir/`test'_`testname'"
	capture mkdir "`output_dir'"
	
	capture confirm file "$result_dir/PDFs/`testname' `test'.pdf"
	if _rc!=0 {
		capture noisily {
			* Load Merged Weather-Test Data
			use "$data_extract_dir/Weather-Project/General/`test'.dta", clear
			drop if lg_0==. | result==. | result==0
			duplicates drop 
			
			* At this point, fix any weird result distribution due to unit changes
			if "`test'"=="751-8" { // Neutrophils: average ranges around 3000
				replace result=result*10000000 if inrange(result, 0.0001, 0.001)
				replace result=result*100000 if inrange(result, 0.01, 0.1)
				replace result=result*1000 if inrange(result, 1, 10)
				replace result=result*100 if inrange(result, 10, 100)
			}
			if "`test'"=="742-7" { // Monocytes: average ranges around 200-600
				replace result=result*10000000 if inrange(result, 0.0001, 0.001)
				replace result=result*100000 if inrange(result, 0.001, 0.01)
				replace result=result*1000 if inrange(result, 0.1, 1)
				replace result=result*100 if inrange(result, 1, 10)
			}
			if "`test'"=="731-0" { // Lymphocytes: average ranges around 1500
				replace result=result*100000 if result<.09
				replace result=result*1000 if inrange(result, .09, 10)
				replace result=result*100 if inrange(result, 10, 100)
			}
			if "`test'"=="704-7" { // Basophils: average ranges around 30
				drop if result<.1 // a lot of ~0.1 results, drop
				replace result=result*100 if inrange(result, .1, 1)
			}
			if "`test'"=="711-2" { // Eosinophils: average ranges around 100 (normal is less than 500)
				replace result=result*100000 if inrange(result, 0, .01)
				replace result=result*1000 if inrange(result, 0.09, 1)
			}
			if "`test'"=="18262-6" { // LDL by Direct Assay: average ranges around 100
				drop if result<1 // a lot of low results, drop
			}
			if "`test'"=="43396-1" { // Cholesterol NON-HDL: average ranges around 130
				drop if result<10 // a lot of low results, drop
			}
			if "`test'"=="2500-7" { // Iron binding capacity: average ranges around 300
				drop if result<50 // a lot of low results, drop
			}
			if "`test'"=="2161-8" { // Creatinine in Urine: average ranges around 100
				replace result=result*100 if result<5
			}
			if "`test'"=="5334-8" { // Rubella virus: average ranges around 20; CHECK WITH DEVIN
				replace result=result*10 if result<5
			}
			if "`test'"=="3209-4" { // Coagulation factor VIII
				replace result=result*10 if result<10
			}
			
			* (1) Trim outliers - Limit by percentile
			_pctile result, percentiles($TRIM_PERC_LOW $TRIM_PERC_HIGH)
			keep if result >= `r(r1)' & result <= `r(r2)'
			
			* (2) Trim to MSAs \\ $TOP_MSA = 0 indicates no MSA trimming
			drop if msa==. | msa==0
		
		}
		
		*Make PDF page for each test with 
			// (1) loinc code, unit measurement, total tests, unique individuals
			// (2) Result by Month graph
			// (3) Result by Temp Bin with MSA*Week and Patient Fixed Effect graph
			// (4) Lead-lag graph
		
		count
		local total: di %9.0fc `r(N)'
		sort enrolid date
		count if enrolid!=enrolid[_n-1]
		local unique: di %9.0fc `r(N)'
		use "$data_extract_dir/Test Units/all_test_units.dta", clear
		keep if loinccd=="`test'"
		local unit = resunit[1]
		
		capture noisily {
			putpdf clear
			putpdf begin, margin(left,1) margin(right,.5) margin(left,.5) margin(top,1) margin(bottom,.5)
			putpdf paragraph, halign(center)
			putpdf text ("eFigure `count'. `testname'"), bold font(, 12) 
			putpdf paragraph, halign(center)
			putpdf text ("Loinc Code: `test'  |  Total Tests: `total'  |  Unique Individuals: `unique'  |  Measurement Units: `unit'"), font(, 9) 
			putpdf table tbl1 = (3,2), border(all,nil) halign(center) width(9)
			putpdf table tbl1(1,1)=image("`output_dir'/Result-Distribution.png")
			putpdf table tbl1(1,2)=image("`output_dir'/Result-By-Week-PatientYear-Cluster.png")
			putpdf table tbl1(2,1)=image("`output_dir'/Result-By-Bucket-MSAWeekPatientYear-Cluster.png")
			putpdf table tbl1(2,2)=image("`output_dir'/Result-LeadLag-MSAWeekPatientYear-Cluster.png")
			putpdf table tbl1(3,1)=image("`output_dir'/Result-By-Bucket-MSAWeekPatientYear-Cluster-Age.png")
			putpdf table tbl1(3,2)=image("$result_dir/Apparent-Temperature/`test'_`testname'/Result-By-Bucket-MSAWeekPatientYear-Cluster.png")
			putpdf save "$result_dir/PDFs/`testname' `test'.pdf", replace
		}
	}
	
	local count= `count'+1
	
}


*_______________________________________________________________________________

*--- CREATE OTHER FIGURES ---*	

* Figure 1
putpdf clear
putpdf begin, margin(left,1) margin(right,.5) margin(left,.5) margin(top,.9) margin(bottom,.9)
putpdf paragraph, halign(left)
putpdf text ("Figure 1."), bold font(, 12) 
putpdf text ("Estimates and 95% confidence intervals (95% CI) for the impact of temperature on laboratory studies reflecting intravascular volume. The effect of temperature on the day of the test (e.g. temperature falling into the range 15-20°C) is shown relative to the coldest days (<5°C, the omitted category, shown without 95% CI). "), font(, 12) 
putpdf paragraph, halign(center)
putpdf table tbl1 = (4,2), border(all,nil) halign(center) width(9)
putpdf table tbl1(1,1)=("a. Creatinine in blood"), halign(left)
putpdf table tbl1(1,2)=("b. Creatinine in Urine"), halign(left)
putpdf table tbl1(3,1)=("c. Urea Nitrogen"), halign(left)
putpdf table tbl1(3,2)=("d. Specific Gravity of Urine"), halign(left)
putpdf table tbl1(2,1)=image("$result_dir/2160-0_Creatinine/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl1(2,2)=image("$result_dir/2161-8_Creatinine in Urine/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl1(4,1)=image("$result_dir/3094-0_Urea Nitrogen/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl1(4,2)=image("$result_dir/5811-5_specific gravity of urine/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf pagebreak
* Figure 2
putpdf paragraph, halign(left)
putpdf text ("Figure 2."), bold font(, 12) 
putpdf text ("Estimates and 95% confidence intervals for the impact of temperature on laboratory studies related to the CBC, lipids, and other liver products. The effect of temperature on the day of the test (e.g. temperature falling into the range 15-20°C) is shown relative to the coldest days (<5°C, the omitted category, shown without 95% CI). "), font(, 12) 
putpdf paragraph, halign(center)
putpdf table tbl2 = (10,2), border(all,nil) halign(center) width(9)
putpdf table tbl2(1,1)=("a. Total Cholesterol"), halign(left)
putpdf table tbl2(1,2)=("b. HDL Cholesterol"), halign(left)
putpdf table tbl2(3,1)=("c. LDL Cholesterol"), halign(left)
putpdf table tbl2(3,2)=("d. Triglycerides"), halign(left)
putpdf table tbl2(2,1)=image("$result_dir/2093-3_Total Cholesterol/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl2(2,2)=image("$result_dir/2085-9_Cholesterol in HDL/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl2(4,1)=image("$result_dir/13457-7_Cholesterol in LDL/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl2(4,2)=image("$result_dir/2571-8_Triglyceride/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf pagebreak
putpdf table tbl3 = (10,2), border(all,nil) halign(center) width(9)
putpdf table tbl3(1,1)=("e. Albumin"), halign(left)
putpdf table tbl3(1,2)=("f. Globulin"), halign(left)
putpdf table tbl3(3,1)=("g. Alanine Aminotransferase"), halign(left)
putpdf table tbl3(3,2)=("h. Aspartate Aminotransferase"), halign(left)
putpdf table tbl3(5,1)=("i. Alkaline Phosphatase"), halign(left)
putpdf table tbl3(5,2)=("j. Bilirubin Total"), halign(left)
putpdf table tbl3(2,1)=image("$result_dir/1751-7_Albumin/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl3(2,2)=image("$result_dir/10834-0_Globulin/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl3(4,1)=image("$result_dir/1742-6_Alanine Aminotransferase/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl3(4,2)=image("$result_dir/1920-8_Aspartate Aminotransferase/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl3(6,1)=image("$result_dir/6768-6_Alkaline Phosphatase/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl3(6,2)=image("$result_dir/1975-2_Bilirubin Total/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf pagebreak
* Figure 3
putpdf paragraph, halign(left)
putpdf text ("Figure 3."), bold font(, 12)
putpdf text ("Estimates and 95% confidence intervals for the impact of temperature on laboratory studies on cellular components of the blood. The effect of temperature on the day of the test (e.g. temperature falling into the range 15-20°C) is shown relative to the coldest days (<5°C, the omitted category, shown without 95% CI). "), font(, 12) 
putpdf paragraph, halign(center)
putpdf table tbl4 = (6,2), border(all,nil) halign(center) width(9)
putpdf table tbl4(1,1)=("a. White Blood Cells (Leukocytes)"), halign(left)
putpdf table tbl4(1,2)=("b. Red Blood Cells (Erythrocytes)"), halign(left)
putpdf table tbl4(3,1)=("c. Erythrocyte Distribution Width"), halign(left)
putpdf table tbl4(3,2)=("d. Hemoglobin"), halign(left)
putpdf table tbl4(2,1)=image("$result_dir/6690-2_Leukocytes/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl4(2,2)=image("$result_dir/789-8_Erythrocytes/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl4(4,1)=image("$result_dir/788-0_Erythrocyte Distribution Width/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf table tbl4(4,2)=image("$result_dir/718-7_Hemoglobin/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf pagebreak
putpdf table tbl5 = (2,2), border(all,nil) halign(center) width(9)
putpdf table tbl5(1,1)=("d. Mean Corpuscular Volume"), halign(left)
putpdf table tbl5(2,1)=image("$result_dir/787-2_Erythrocyte mean corpuscular/Result-By-Bucket-MSAWeekPatientYear-Cluster-NoTitle.png")
putpdf save "$result_dir/PDFs/0-Figures.pdf", replace






