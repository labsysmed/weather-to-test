/*______________________________________________________________________________

	Regression Table (Linear)
	- MSA*Week, Patient, Year FE
	- Cluster by MSA*Date
	Scripts/Cleaned-Weather-Project/General/
______________________________________________________________________________*/

version 14
clear all
set more off
capture log close

* ------------------------------------------------------------ *
* Loop through tests and do analysis
* ------------------------------------------------------------ *

capture log close
log using "$log_dir/Replicate-Devin-Linear.smcl", replace
	
foreach test in $TopTests {

	* Find test name
	use "$crosswalk_path" if loinccd == "`test'", clear
	local testname = test[1]
	di "Processing test `test': `testname'"

	* ------------------------------------------------------------ *
	* More Data Cleaning: Generate Data Trimming Flags
	* (1) Trim outliers by percentile
	* (2) Drop missing values
	* ------------------------------------------------------------ *
	capture qui {
		* Load Merged Weather-Test Data
		use "$data_extract_dir/Weather-Project/General/`test'.dta", clear
		drop if lg_0==. | result==. | result==0
		duplicates drop 
		
		* At this point, fix any weird result distribution due to unit changes
		if "`test'"=="751-8" { // Neutrophils: average ranges around 3000
			replace result=result*10000000 if inrange(result, 0.0001, 0.001)
			replace result=result*100000 if inrange(result, 0.01, 0.1)
			replace result=result*1000 if inrange(result, 1, 10)
			replace result=result*100 if inrange(result, 10, 100)
		}
		if "`test'"=="742-7" { // Monocytes: average ranges around 200-600
			replace result=result*10000000 if inrange(result, 0.0001, 0.001)
			replace result=result*100000 if inrange(result, 0.001, 0.01)
			replace result=result*1000 if inrange(result, 0.1, 1)
			replace result=result*100 if inrange(result, 1, 10)
		}
		if "`test'"=="731-0" { // Lymphocytes: average ranges around 1500
			replace result=result*100000 if result<.09
			replace result=result*1000 if inrange(result, .09, 10)
			replace result=result*100 if inrange(result, 10, 100)
		}
		if "`test'"=="704-7" { // Basophils: average ranges around 30
			drop if result<.1 // a lot of ~0.1 results, drop
			replace result=result*100 if inrange(result, .1, 1)
		}
		if "`test'"=="711-2" { // Eosinophils: average ranges around 100 (normal is less than 500)
			replace result=result*100000 if inrange(result, 0, .01)
			replace result=result*1000 if inrange(result, 0.09, 1)
		}
		if "`test'"=="18262-6" { // LDL by Direct Assay: average ranges around 100
			drop if result<1 // a lot of low results, drop
		}
		if "`test'"=="43396-1" { // Cholesterol NON-HDL: average ranges around 130
			drop if result<10 // a lot of low results, drop
		}
		if "`test'"=="2500-7" { // Iron binding capacity: average ranges around 300
			drop if result<50 // a lot of low results, drop
		}
		if "`test'"=="2161-8" { // Creatinine in Urine: average ranges around 100
			replace result=result*100 if result<5
		}
		if "`test'"=="5334-8" { // Rubella virus: average ranges around 20; CHECK WITH DEVIN
			replace result=result*10 if result<5
		}
		if "`test'"=="3209-4" { // Coagulation factor VIII
			replace result=result*10 if result<10
		}
		
		* (1) Trim outliers - Limit by percentile
		_pctile result, percentiles($TRIM_PERC_LOW $TRIM_PERC_HIGH)
		gen to_keep = result >= `r(r1)' & result <= `r(r2)'
		* At this moment, drop bad observations
		capture noisily drop if to_keep == 0  
		
		* (2) Trim to MSAs \\ $TOP_MSA = 0 indicates no MSA trimming
		drop if msa==. | msa==0
		gen top_msa_flag = 1
		
		* Get month and week of year
		gen year = year(date)
		gen week = week(date)
	}
	
	* ------------------------------------------------------------ *
	* Temperature bins
	* ------------------------------------------------------------ *
	capture qui {	
		* Convert temp into Celsius
		replace lg_0=(lg_0-32)*(5/9)
		foreach num of numlist 1/15 {
			replace lg_`num'=(lg_`num'-32)*(5/9)
			replace ld_`num'=(ld_`num'-32)*(5/9)
		}
		* Binning the temperature
		gen bin = $TEMPERATURE_BIN*floor(lg_0/$TEMPERATURE_BIN)
		replace bin = 0 if bin < 0
		replace bin = 35 if bin > 35
	}
	* ------------------------------------------------------------ *
	* Analysis 2: Result on temperature bins, patient + msa*week FE, cluster
	* ------------------------------------------------------------ *
	eststo: reghdfe result lg_0, absorb(msa#week enrolid year) vce(cluster msa#date) tol($TOL_HDFE) poolsize(20) accel(sd)
}

* Output all regressions into table
esttab using "$result_dir/linear-regs.csv", replace label b(4) se(4) ar2












































